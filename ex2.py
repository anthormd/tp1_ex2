
class SimpleCalculator:


    def __init__(self,a,b):
        self.a = a
        self.b = b 


    def addition(self):
        return self.a + self.b

    def soustraction(self):
        return self.a - self.b

    def produit(self):
        return self.a*self.b

    def quotient(self):
        if self.b!= 0:
            return self.a/self.b
        else:
            return 'pas de division par 0'


test = SimpleCalculator(27,36)
print(test.addition())
print(test.soustraction())
print(test.produit())
print(test.quotient())